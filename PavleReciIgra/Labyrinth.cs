﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace PavleReciIgra
{
    public class Labyrinth : IGame
    {
        /// <summary>
        /// Matrix structure
        /// (0, 0) --------- y (width)
        /// |
        /// |
        /// |
        /// x (height)
        /// </summary>
        private class CurrentPosition
        {
            public int X { get; set; }
            public int Y { get; set; }

            public override bool Equals(object obj)
            {
                if (!(obj is Labyrinth))
                {
                    return false;
                }

                CurrentPosition other = (CurrentPosition)obj;
                return this.X == other.X && this.Y == other.Y;
            }
        }

        private enum Direction
        {
            Left,
            Right,
            Up,
            Down,
        }

        private bool[][] labyrinth;
        private CurrentPosition pos = new CurrentPosition();
        private int width = 200;
        private int height = 40;

        private void MoveDirecttion(CurrentPosition cp, Direction dir, int steps)
        {
            if (dir == Direction.Down)
            {
                for (int p = 1; p <= steps; p++)
                    this.labyrinth[cp.X + p][cp.Y] = true;
                cp.X += steps;
                if (cp.X > height)
                {
                    throw new ArgumentException();
                }
            }
            else if (dir == Direction.Up)
            {
                for (int p = 1; p <= steps; p++)
                    this.labyrinth[cp.X - p][cp.Y] = true;
                cp.X -= steps;
                if (cp.X < 0)
                {
                    throw new ArgumentException();
                }
            }
            else if (dir == Direction.Left)
            {
                for (int p = 1; p <= steps; p++)
                    this.labyrinth[cp.X][cp.Y - p] = true;
                cp.Y -= steps;
                if (cp.Y < 0)
                {
                    throw new ArgumentException();
                }
            }
            else if (dir == Direction.Right)
            {
                for (int p = 1; p <= steps; p++)
                    this.labyrinth[cp.X][cp.Y + p] = true;
                cp.Y += steps;
                if (cp.Y > this.width)
                {
                    throw new ArgumentException();
                }
            }
            else
            {
                throw new InvalidProgramException();
            }
        }

        private CurrentPosition MoveRandomAlreadVisited(CurrentPosition cp, Random rnd)
        {
            var points = new List<(int, int)>();
            for (int ii = 0; ii < this.height; ii++)
                for (int jj = 0; jj < this.width; jj++)
                {
                    if (this.labyrinth[ii][jj])
                    {
                        points.Add((ii, jj));
                    }
                }

            (int i, int j) = points.ElementAt(rnd.Next(0, points.Count));
            cp.X = i;
            cp.Y = j;

            return cp;
        }

        private (Direction, int) ChooseMovement(CurrentPosition cp, Random rnd)
        {
            List<Direction> dirs = new List<Direction>();

            int offsetFromEdge = rnd.Next(5, 10);

            if (cp.X > offsetFromEdge)
            {
                dirs.Add(Direction.Up);

                bool pathNotTaken = true;
                for (int i = 1; i <= offsetFromEdge; i++)
                {
                    if (this.labyrinth[cp.X-i][cp.Y] == true)
                    {
                        pathNotTaken = false;
                        break;
                    }
                }

                if (pathNotTaken)
                {
                    for (int i = 0; i < 40; i++)
                    {
                        dirs.Add(Direction.Up);
                    }
                }
            }

            if (cp.X < this.height - offsetFromEdge)
            {
                dirs.Add(Direction.Down);

                bool pathNotTaken = true;
                for (int i = 1; i <= offsetFromEdge; i++)
                {
                    if (this.labyrinth[cp.X+i][cp.Y] == true)
                    {
                        pathNotTaken = false;
                        break;
                    }
                }

                if (pathNotTaken)
                {
                    for (int i = 0; i < 50; i++)
                    {
                        dirs.Add(Direction.Down);
                    }
                }
            }

            if (cp.Y > offsetFromEdge)
            {
                dirs.Add(Direction.Left);

                bool pathNotTaken = true;
                for (int i = 1; i <= offsetFromEdge; i++)
                {
                    if (this.labyrinth[cp.X][cp.Y - i] == true)
                    {
                        pathNotTaken = false;
                        break;
                    }
                }

                if (pathNotTaken)
                {
                    for (int i = 1; i < 30; i++)
                    {
                        dirs.Add(Direction.Left);
                    }
                }
            }

            if (cp.Y < this.width - offsetFromEdge)
            {
                dirs.Add(Direction.Right);

                bool pathNotTaken = true;
                for (int i = 1; i <= offsetFromEdge; i++)
                {
                    if (this.labyrinth[cp.X][cp.Y + i] == true)
                    {
                        pathNotTaken = false;
                        break;
                    }
                }

                if (pathNotTaken)
                {
                    for (int i = 0; i < 50; i++)
                    {
                        dirs.Add(Direction.Right);
                    }
                }
            }

            if (dirs.Count() < 10 && rnd.Next(0, 4) == 0)
            {
                // Move random
                cp = this.MoveRandomAlreadVisited(cp, rnd);
                return this.ChooseMovement(cp, rnd);
            }
            else
            {
                Direction dir = dirs.ElementAt(rnd.Next(0, dirs.Count()));

                // get steps.
                int steps = rnd.Next(1, offsetFromEdge);
                return (dir, steps);
            }

        }

        private void MoveToSolution(CurrentPosition cp)
        {
            for (int i = 0; i < cp.X; i++)
            {
                this.labyrinth[i][cp.Y] = true;
            }

            for (int i = 0; i < cp.Y; i++)
            {
                this.labyrinth[0][i] = true;
            }
        }

        private void GenerateLabyrinth()
        {
            // start at lower, right corner.
            CurrentPosition cp = new CurrentPosition();
            CurrentPosition fp = new CurrentPosition();
            cp.X = this.height- 1;
            cp.Y = this.width - 1;
            fp.X = 0;
            fp.Y = 0;

            this.labyrinth = new bool[this.height][];
            for (int i = 0; i < this.height; i++)
            {
                this.labyrinth[i] = new bool[this.width];
            }


            for (int i = 0; i < this.height; i++)
            {
                for (int j = 0; j < this.width; j++)
                {
                    this.labyrinth[i][j] = false;
                }
            }

            this.labyrinth[cp.X][cp.Y] = true;

            bool finished = false;

            Random rnd = new Random();

            while (!finished)
            {
                // Move one step in given direction.
                (Direction currDirection, int steps) = this.ChooseMovement(cp, rnd);
                this.MoveDirecttion(cp, currDirection, steps);

                if (cp.X < 10 && cp.Y < 10)
                {
                    // Just find shortest route from here.
                    this.MoveToSolution(cp);
                    finished = true;
                }
            }

            int cntFree = 0;
            for (int i = 0; i < height; i++)
                for (int j = 0; j < width; j++)
                {
                    if (this.labyrinth[i][j] == true)
                    {
                        cntFree++;
                    }
                }

            int limit = this.height * this.width / 4;
            if (cntFree > limit)
            {
                // try again.
                this.GenerateLabyrinth();
            }
            else
            {
                DrawLabyrinth();
            }

        }

        private void RemoveOldPosition(CurrentPosition cp)
        {
            Console.SetCursorPosition(cp.Y, cp.X);
            Console.Write(" ");
        }

        private void DrawNewPosition(CurrentPosition cp)
        {
            Console.SetCursorPosition(cp.Y, cp.X);
            Console.BackgroundColor = ConsoleColor.Red;
            Console.Write("O");
            Console.BackgroundColor = ConsoleColor.Black;
        }

        private void DrawLabyrinth()
        {
            Console.WindowHeight = this.height + 5;
            Console.WindowWidth = this.width + 1;
            Console.OutputEncoding = System.Text.Encoding.Unicode;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.BackgroundColor = ConsoleColor.Black;

            for (int i = 0; i < this.height; i++)
            {
                for (int j = 0; j < this.width; j++)
                {
                    if (this.pos.X == i && this.pos.Y == j)
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.Write("O");
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                    else if (this.labyrinth[i][j])
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.Write("\u2573");
                    }
                }

                Console.WriteLine();
            }
        }

        private void DrawVictory()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("BRAVO!!!!!!!!!!!!!");
            Console.WriteLine("Uspesno ste resili lavirint!!!");
            Console.WriteLine("Pritisnite bilo koje dugme da se generise novi.");
            Console.ReadKey();
        }

        public void GameLoop()
        {
            while (true)
            {
                this.pos = new CurrentPosition();
                pos.X = this.height - 1;
                pos.Y = this.width - 1;
                GenerateLabyrinth();

                bool gameFinished = false;
                while (!gameFinished)
                {
                    while (!Console.KeyAvailable)
                    {
                        // no op.
                        System.Threading.Thread.Sleep(10);
                    }

                    ConsoleKey key = Console.ReadKey(true).Key;
                    bool moved = false;

                    this.RemoveOldPosition(this.pos);

                    if (key == ConsoleKey.UpArrow)
                    {
                        if (this.pos.X > 0 && this.labyrinth[this.pos.X - 1][this.pos.Y] == true)
                        {
                            this.pos.X--;
                            moved = true;
                        }
                    }
                    else if (key == ConsoleKey.DownArrow)
                    {
                        if (this.pos.X < this.height - 2 && this.labyrinth[this.pos.X + 1][this.pos.Y] == true)
                        {
                            this.pos.X++;
                            moved = true;
                        }
                    }
                    else if (key == ConsoleKey.LeftArrow)
                    {
                        if (this.pos.Y > 0 && this.labyrinth[this.pos.X][this.pos.Y - 1] == true)
                        {
                            this.pos.Y--;
                            moved = true;
                        }
                    }
                    else if (key == ConsoleKey.RightArrow)
                    {
                        if (this.pos.Y < this.width - 2 && this.labyrinth[this.pos.X][this.pos.Y + 1] == true)
                        {
                            this.pos.Y++;
                            moved = true;
                        }
                    }
                    else if (key == ConsoleKey.Enter)
                    {
                        gameFinished = true;
                    }

                    this.DrawNewPosition(this.pos);

                    if (this.pos.X == 0 && this.pos.Y == 0)
                    {
                        this.DrawVictory();
                        gameFinished = true;
                    }
                }
            }
        }
    }
}
