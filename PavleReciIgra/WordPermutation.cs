﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PavleReciIgra
{
    public class WordPermutation : IGame
    {
        private static string[] words =
            new[] { 
                "MAMA", "TATA", "DEDA", "ZOMBI", "TELEVIZOR", "DOBI", "AUTO",
                "KOMPJUTER", "KALEMEGDAN", "MONOPOL", "TIGAR", "DINOSAURUS", "KROKODIL",
                "SLON", "ZMIJA", "RAKETA", "SUNCE", "MESEC", "ZOJA",
                "ZMAJ", "PARK", "ASTERIKS", "OBELIKS", "MIKI",
                "PAJA", "PONI", "REJMEN", "LISICA", "AUTOBUS", "AVION",
                "KOCKA", "KRUG", "DUBROVNIK", "KOPAONIK", "ANDREJ",
                "NIKOLA", "MAKI", "ZOKI", "NOGA", "RUKA", "GLAVA",
                "STOMAK", "NOKAT", "ULICA", "TASTATURA", "BANANA"
            };

        public void GameLoop()
        {
            int points = 0;
            while (true)
            {
                Random r = new Random();
                int wordIndex = r.Next(0, words.Length - 1);

                string word = words[wordIndex];

                string permutation = new string(word.ToCharArray().OrderBy(x => r.Next()).ToArray());

                while (true)
                {
                    Console.WriteLine("Koja je ovo rec?");
                    Console.WriteLine(permutation);
                    string input = Console.ReadLine().ToUpper();
                    if (input == word)
                    {
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("BRAVO!!!!");
                        Console.WriteLine("Imate {0} poena", ++points);
                        Console.ForegroundColor = ConsoleColor.White;
                        break;
                    }
                    else if (input.ToUpper() == "VELIKA POMOC")
                    {
                        Console.WriteLine("Evo ga odgovor. Trazena rec je:");
                        Console.WriteLine(word);
                        Console.WriteLine("Broj poena se manjio na {0}", --points);
                        break;
                    }
                    else if (input.ToUpper() == "MALA POMOC")
                    {
                        Console.WriteLine("Prva dva slova su:");
                        Console.WriteLine(new string(word.Take(2).ToArray()));
                    }
                    else if (input.ToUpper() == "SREDNJA POMOC")
                    {
                        Console.WriteLine("Prva tri slova su:");
                        Console.WriteLine(new string(word.Take(3).ToArray()));
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Netacno. Pokusajte ponovo.");
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                }
            }
        }
    }
}
