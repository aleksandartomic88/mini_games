﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace PavleReciIgra
{
    class Hangman : IGame
    {

        static string frame0 = @"
|---------------|
|               |
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|
|";

        static string frame1 = @"
|----------------|
|                |
|             ________
|             |      |
|             |      |
|             |      |
|             |      |
|             |______|
|                |
|
|
|
|
|
|
|
|
|";
        static string frame2 = @"
|----------------|
|                |
|             ________
|             |      |
|             | o  o |
|             |      |
|             |      |
|             |______|
|                |
|
|
|
|
|
|
|
|
|";
        static string frame3 = @"
|----------------|
|                |
|             ________
|             |      |
|             | o  o |
|             |      |
|             | ---- |
|             |______|
|                |
|
|
|
|
|
|
|
|
|";

        static string frame4 = @"
|----------------|
|                |
|             ________
|             |      |
|            O| o  o |O
|             |      |
|             | ---- |
|             |______|
|                |
|
|
|
|
|
|
|
|
|";
        static string frame5 = @"
|----------------|
|                |
|             ________
|             |      |
|            O| o  o |O
|             |      |
|             | ---- |
|             |______|
|                |
|             _______
|             |     |
|             |     |
|             |     |
|             |     |
|             |+++++|
|
|
|
";

        static string frame6 = @"
|----------------|
|                |
|             ________
|             |      |
|            O| o  o |O
|             |      |
|             | ---- |
|             |______|
|                |
|             _______
|            /|     |
|           / |     |
|           | |     |
|           | |     |
|             |+++++|
|
|
|
";

        static string frame7 = @"
|----------------|
|                |
|             ________
|             |      |
|            O| o  o |O
|             |      |
|             | ---- |
|             |______|
|                |
|             _______
|            /|     |\
|           / |     | \
|           | |     |  |
|           | |     |  |
|             |+++++|
|
|
|
";

        static string frame8 = @"
|----------------|
|                |
|             ________
|             |      |
|            O| o  o |O
|             |      |
|             | ---- |
|             |______|
|                |
|             _______
|            /|     |\
|           / |     | \
|           | |     |  |
|           | |     |  |
|             |+++++|
|             |
|             |
|
";

        static string frame9 = @"
|----------------|
|                |
|             ________
|             |      |
|            O| o  o |O
|             |      |
|             | ---- |
|             |______|
|                |
|             _______
|            /|     |\
|           / |     | \
|           | |     |  |
|           | |     |  |
|             |+++++|
|             |     |
|             |     |
|
";

        static string frame10 = @"
|----------------|
|                |
|             ________
|             |      |
|            O| x  x |O
|             |      |
|             | xxxx |
|             |______|
|                |
|             _______
|            /|     |\
|            ||     ||
|            ||     ||
|            ||     ||
|             |+++++|
|             |     |
|          ^^^|     | ^^^
|          ^^^^^^^^^^^^^^
";

        private string[] NormalizeFrame(string frame, int targetWidth)
        {
            string[] split = frame.Split("\r\n");
            List<string> normalized = new List<string>();

            foreach (string curr in split)
            {
                int spacesLeft = targetWidth - curr.Length;
                string spacesToAdd = new string(Enumerable.Repeat(' ', spacesLeft).ToArray());
                normalized.Add(curr + spacesToAdd);
            }

            return normalized.ToArray();
        }

        private void PrintFrame(int posX, int posY, string[] frame)
        {
            Console.SetCursorPosition(posX, posY);
            int offset = 0;
            foreach (string line in frame)
            {
                Console.Write(line);
                offset++;
                Console.SetCursorPosition(posX, posY + offset);
            }
        }

        private void PrintLetterPositions(int posX, int posY, int count)
        {
            for (int i = 0; i < count; i++)
            {
                Console.SetCursorPosition(posX + i * 2, posY);
                Console.WriteLine("_");
            }
        }

        private void PrintFoundLetters(int posX, int posY, string word, bool[] lettersFound)
        {
            for (int i = 0; i < lettersFound.Length; i++)
            {
                if (lettersFound[i] == true)
                {
                    char c = word[i];
                    Console.SetCursorPosition(posX + i * 2, posY);
                    Console.WriteLine(c);
                }
            }
        }

        private bool UpdateFoundMask(string word, bool[] foundMask, char key)
        {
            bool found = false;

            for (int i = 0; i < word.Length; i++)
            {
                if (word[i] == key)
                {
                    foundMask[i] = true;
                    found = true;
                }
            }

            return found;
        }

        private void PrintPoints(int points, int posX, int posY)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.SetCursorPosition(posX, posY);
            Console.Write("Poeni: ");
            Console.Write(points);
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static string[] words =
            new[] { 
                "MAMA", "TATA", "DEDA", "ZOMBI", "TELEVIZOR", "DOBI", "AUTO",
                "KOMPJUTER", "KALEMEGDAN", "MONOPOL", "TIGAR", "DINOSAURUS", "KROKODIL",
                "SLON", "ZMIJA", "RAKETA", "SUNCE", "MESEC", "ZOJA",
                "ZMAJ", "PARK", "ASTERIKS", "OBELIKS", "MIKI",
                "PAJA", "PONI", "REJMEN", "LISICA", "AUTOBUS", "AVION",
                "KOCKA", "KRUG", "DUBROVNIK", "KOPAONIK", "ANDREJ",
                "NIKOLA", "MAKI", "ZOKI", "NOGA", "RUKA", "GLAVA",
                "STOMAK", "NOKAT", "ULICA", "TASTATURA", "BANANA"
            };

        public void GameLoop()
        {
            const int hangmanWidth = 30;
            string[][] frames = (new[] { frame0, frame1, frame2, frame3, frame4, frame5, frame6, frame7, frame8, frame9, frame10 }).Select(f => NormalizeFrame(f, hangmanWidth)).ToArray();

            const int letterPositionStartLeft = hangmanWidth + 5;
            const int letterPositionStartDown = 10;
            int points = 0;

            while (true)
            {
                Console.Clear();
                Console.CursorVisible = false;

                Random r = new Random();
                int wordIndex = r.Next(0, words.Length - 1);

                string word = words[wordIndex];
                bool[] foundMask = Enumerable.Repeat(false, word.Length).ToArray();

                int misses = 0;

                bool gameCompleted = false;
                while (!gameCompleted)
                {
                    PrintLetterPositions(letterPositionStartLeft, letterPositionStartDown, word.Length);
                    PrintFoundLetters(letterPositionStartLeft, letterPositionStartDown - 1, word, foundMask);
                    PrintFrame(1, 1, frames[misses]);
                    PrintPoints(points, letterPositionStartLeft, letterPositionStartDown - 3);

                    if (!foundMask.Any(f => f == false))
                    {
                        Console.Clear();
                        Console.WriteLine("POBEDA!!!");
                        Console.ReadKey();
                        points++;
                        gameCompleted = true;
                    }
                    else if (misses == frames.Length - 1)
                    {
                        gameCompleted = true;
                        for (int i = 0; i < foundMask.Length; i++)
                        {
                            foundMask[i] = true;
                        }

                        PrintFoundLetters(letterPositionStartLeft, letterPositionStartDown - 1, word, foundMask);
                        PrintFoundLetters(letterPositionStartLeft, letterPositionStartDown - 3, "KRAJ", new bool[] { true, true, true, true });

                        points = Math.Max(0, points - 1);
                        PrintPoints(points, letterPositionStartLeft, letterPositionStartLeft - 2);
                        Console.ReadKey();
                    }
                    else
                    {
                        char key = char.ToUpper(Console.ReadKey(true).KeyChar);
                        bool charFound = UpdateFoundMask(word, foundMask, key);

                        if (!charFound)
                        {
                            misses++;
                        }
                    }

                }
            }
        }
    }
}
