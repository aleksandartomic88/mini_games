﻿using System;

namespace PavleReciIgra
{
    class Program
    {
        static Tuple<string, Func<IGame>>[] games = new[]
        {
            Tuple.Create<string, Func<IGame>>("1) Igra reci", (() => new WordPermutation())),
            Tuple.Create<string, Func<IGame>>("2) Lavirint", (() => new Labyrinth())),
            Tuple.Create<string, Func<IGame>>("3) Vesanje", (() => new Hangman())),
        };

        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Izaberite igru:");
                foreach (var game in games)
                {
                    Console.WriteLine(game.Item1);
                }

                Console.WriteLine("Unesite broj igre:");
                string num = Console.ReadLine();

                if (Int32.TryParse(num, out int res))
                {
                    if (res < 1 || res > games.Length)
                    {
                        Console.WriteLine("Nevalidan broj.");
                    }
                    else
                    {
                        IGame game = games[res-1].Item2();
                        game.GameLoop();
                    }
                }
                else
                {
                    Console.WriteLine("Nepoznat izbor.");
                }
            }
        }
    }
}
