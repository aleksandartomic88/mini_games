from random import randint, uniform
from math import pi, sqrt, cos, sin, atan2
import vectors
import numpy as np
from vectors import distance

def add(*vectors):
    return tuple(map(sum,zip(*vectors)))

def subtract(v1,v2):
    return tuple(v1-v2 for (v1,v2) in zip(v1,v2))

def length(v):
    return sqrt(sum([coord ** 2 for coord in v]))

def dot(u,v):
    return sum([coord1 * coord2 for coord1,coord2 in zip(u,v)])

def distance(v1,v2):
    return length(subtract(v1,v2))

def perimeter(vectors):
    distances = [distance(vectors[i], vectors[(i+1)%len(vectors)])
                    for i in range(0,len(vectors))]
    return sum(distances)

def scale(scalar,v):
    return tuple(scalar * coord for coord in v)

def to_cartesian(polar_vector):
    length, angle = polar_vector[0], polar_vector[1]
    return (length*cos(angle), length*sin(angle))

def rotate2d(angle, vector):
    l,a = to_polar(vector)
    return to_cartesian((l, a+angle))

def translate(translation, vectors):
    return [add(translation, v) for v in vectors]

def to_polar(vector):
    x, y = vector[0], vector[1]
    angle = atan2(y,x)
    return (length(vector), angle)

def angle_between(v1,v2):
    return acos(
                dot(v1,v2) /
                (length(v1) * length(v2))
            )

def cross(u, v):
    ux,uy,uz = u
    vx,vy,vz = v
    return (uy*vz - uz*vy, uz*vx - ux*vz, ux*vy - uy*vx)

def component(v,direction):
    return (dot(v,direction) / length(direction))

def unit(v):
    return scale(1./length(v), v)

def linear_combination(scalars,*vectors):
    scaled = [scale(s,v) for s,v in zip(scalars,vectors)]
    return add(*scaled)

def standard_form(v1, v2):
    x1, y1 = v1
    x2, y2 = v2
    a = y2 - y1
    b = x1 - x2
    c = x1 * y2 - y1 * x2
    return a,b,c

def intersection(u1,u2,v1,v2):
    a1, b1, c1 = standard_form(u1,u2)
    a2, b2, c2 = standard_form(v1,v2)
    m = np.array(((a1,b1),(a2,b2)))
    c = np.array((c1,c2))
    return np.linalg.solve(m,c)

## Will fail if lines are parallel!
# def do_segments_intersect(s1,s2):
#     u1,u2 = s1
#     v1,v2 = s2
#     l1, l2 = distance(*s1), distance(*s2)
#     x,y = intersection(u1,u2,v1,v2)
#     return (distance(u1, (x,y)) <= l1 and
#             distance(u2, (x,y)) <= l1 and
#             distance(v1, (x,y)) <= l2 and
#             distance(v2, (x,y)) <= l2)

def segment_checks(s1,s2):
    u1,u2 = s1
    v1,v2 = s2
    l1, l2 = distance(*s1), distance(*s2)
    x,y = intersection(u1,u2,v1,v2)
    return [
        distance(u1, (x,y)) <= l1,
        distance(u2, (x,y)) <= l1,
        distance(v1, (x,y)) <= l2,
        distance(v2, (x,y)) <= l2
    ]

def do_segments_intersect(s1,s2):
    u1,u2 = s1
    v1,v2 = s2
    d1, d2 = distance(*s1), distance(*s2)
    try:
        x,y = intersection(u1,u2,v1,v2)
        return (distance(u1, (x,y)) <= d1 and
                distance(u2, (x,y)) <= d1 and
                distance(v1, (x,y)) <= d2 and
                distance(v2, (x,y)) <= d2)
    except np.linalg.linalg.LinAlgError:
        return False


# print(do_segments_intersect(((0,2),(1,-1)),((0,0),(4,0))))

# a = np.array(((1,0), (0,1)))
# b = np.array((9,0))
# x = np.linalg.solve(a, b)
# print(x)


class PolygonModel():
    def __init__(self,points):
        self.points = points
        self.rotation_angle = 0
        self.x = 0
        self.y = 0
        self.vx = 50
        self.vy = 10
        self.angular_velocity = 0

    def transformed(self):
        # instead of 2d rotate trick I could use here multiplication?
        # not sure what is faster..
        rotated = [rotate2d(self.rotation_angle, v) for v in self.points]
        return [add((self.x,self.y),v) for v in rotated]

    def move(self, milliseconds):
        dx, dy = self.vx * milliseconds / 100.0, self.vy * milliseconds / 100.0
        self.x, self.y = vectors.add((self.x,self.y), (dx,dy))
        self.rotation_angle += self.angular_velocity * milliseconds / 1000.0

    def segments(self):
        point_count = len(self.points)
        points = self.transformed()
        return [(points[i], points[(i+1)%point_count])
                for i in range(0,point_count)]

    def does_collide(self, other_poly):
        for other_segment in other_poly.segments():
            if self.does_intersect(other_segment):
                return True
        return False

    def does_intersect(self, other_segment):
        for segment in self.segments():
            if do_segments_intersect(other_segment,segment):
                return True
        return False

class Ship(PolygonModel):
    def __init__(self):
        super().__init__([(0.5,0), (-0.25,0.25), (-0.25,-0.25)])

    def laser_segment(self):
        dist = 20. * sqrt(2)
        x,y = self.transformed()[0]
        return (x,y), (x + dist * cos(self.rotation_angle), y + dist*sin(self.rotation_angle))

    def move_ship(self, milliseconds, screen_end):
        xx,yy = self.transformed()[0]
        secondsDiv = 10 ** 7
        dx, dy = (cos(self.rotation_angle) * milliseconds) / secondsDiv, (sin(self.rotation_angle) * milliseconds) / secondsDiv 
        self.x = xx + dx
        self.y = yy + dy
        if (super().does_collide(screen_end)):
            self.x -= 20 * dx
            self.y -= 20 * dy
            return

        if (self.x > 10 or self.x < -10 or self.y > 10 or self.y < -10):
            self.x = 0
            self.y = 0
            return

        return


class ScreenEnd(PolygonModel):
    def __init__(self):
        super().__init__([(-10, -10), (-10, 10), (10, 10), (10, -10)])

class Asteroid(PolygonModel):
    def __init__(self):
        sides = randint(4, 8)
        vs = [to_cartesian((uniform(0.5,1.0), 2 * pi * i / sides))
                for i in range(0,sides)]
        super().__init__(vs)
        self.vx =  uniform(-0.2,0.2)
        self.vy =  uniform(--0.2,0.2)
        self.angular_velocity = uniform(-pi/2,pi/2)

    def change_direction(self):
        delta = 0.5
        if (abs(abs(self.y) - 10) < delta):
            self.vy= -self.vy
        if (abs(abs(self.x) - 10) < delta):
            self.vx = -self.vx

        #self.vx = -self.vx
        #self.angular_velocity = pi/2 - self.angular_velocity

