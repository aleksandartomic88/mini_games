# Import a library of functions called 'pygame'
import pygame
import vectors
from polygon import *
from math import pi, sqrt, cos, sin, atan2
from random import randint, uniform
from linear_solver import do_segments_intersect
import sys

# DEFINE OBJECTS OF THE GAME

# INITIALIZE GAME STATE

ship = Ship()
screenEnd = ScreenEnd()

asteroid_count = 10
asteroids = [Asteroid() for _ in range(0,asteroid_count)]

def gen_asteroids(asteroids):
    for ast in asteroids:
        notDone = True
        while (notDone):
            ast.x = randint(-9,9)
            ast.y = randint(-9,9)
            if (abs(ast.x) > 5 and abs(ast.y) > 5):
                notDone = False

gen_asteroids(asteroids)

# HELPERS / SETTINGS

BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
BLUE =  (  0,   0, 255)
GREEN = (  0, 255,   0)
RED =   (255,   0,   0)

import ctypes
user32 = ctypes.windll.user32
screensize = user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)
width, height = 800, 800

def to_pixels(x,y):
    return (width/2 + width * x / 20, height/2 - height * y / 20)

def draw_poly(screen, polygon_model, color=GREEN):
    pixel_points = [to_pixels(x,y) for x,y in polygon_model.transformed()]
    pygame.draw.aalines(screen, color, True, pixel_points, 10)

def draw_segment(screen, v1,v2,color=RED):
    pygame.draw.aaline(screen, color, to_pixels(*v1), to_pixels(*v2), 10)

screenshot_mode = False

# INITIALIZE GAME ENGINE
import time

def main():
    pygame.init()

    screen = pygame.display.set_mode([width,height])
    #screen = pygame.display.set_mode((0, 0), pygame.FULLSCREEN)
    #pygame.display.toggle_fullscreen()

    pygame.display.set_caption("Asteroids!")

    done = False
    clock = pygame.time.Clock()
    timeLastLaser = time.time() 
    laserFired = False

    # p key prints screenshot (you can ignore this variable)
    p_pressed = False

    asteroid_count = 10 
    asteroids = [Asteroid() for _ in range(0,asteroid_count)]
    gen_asteroids(asteroids)

    while not done:

        clock.tick()

        for event in pygame.event.get(): # User did something
            if event.type == pygame.QUIT: # If user clicked close
                done=True # Flag that we are done so we exit this loop

        # UPDATE THE GAME STATE

        milliseconds = clock.get_time()
        keys = pygame.key.get_pressed()

        for ast in asteroids:
            ast.move(milliseconds)

        if keys[pygame.K_LEFT]:
            ship.rotation_angle += milliseconds * (2*pi / 1000)

        if keys[pygame.K_RIGHT]:
            ship.rotation_angle -= milliseconds * (2*pi / 1000)
        if keys[pygame.K_UP]:
            ship.move_ship(milliseconds, screenEnd)
        if keys[pygame.K_ESCAPE]:
            done = True

        laser = ship.laser_segment()

        # p key saves screenshot (you can ignore this)
        if keys[pygame.K_p] and screenshot_mode:
            p_pressed = True
        elif p_pressed:
            pygame.image.save(screen, 'figures/asteroid_screenshot_%d.png' % milliseconds)
            p_pressed = False

        # DRAW THE SCENE

        screen.fill((0, 100, 255))

        if keys[pygame.K_SPACE]:
            pressedTime  = time.time()
            timeElapsed = pressedTime - timeLastLaser
            if (timeElapsed > 0.5):
                draw_segment(screen, *laser)
                timeLastLaser = time.time()
                laserFired = True
            elif (timeElapsed < 0.1):
                # no tick here
                draw_segment(screen, *laser)
                laserFired = True
            else:
                laserFired = False

        draw_poly(screen,ship)

        for asteroid in asteroids:
            if (screenEnd.does_collide(asteroid)):
                asteroid.change_direction()
                print("END SCREEN")
            # see if direction needs to change
            ##################################
            if keys[pygame.K_SPACE] and laserFired and asteroid.does_intersect(laser):
                asteroids.remove(asteroid)
                if (len(asteroids) == 0):
                    asteroids = [Asteroid() for _ in range(0,asteroid_count)]
                    gen_asteroids(asteroids)
            else:
                if (ship.does_collide(asteroid)):
                    asteroids = []
                    asteroids = [Asteroid() for _ in range(0,asteroid_count)]
                    gen_asteroids(asteroids)
                    print("COLLISION!!!")

                #
                draw_poly(screen, asteroid, color=GREEN)


        pygame.display.flip()

    pygame.quit()

if __name__ == "__main__":
    if '--screenshot' in sys.argv:
        screenshot_mode = True
    main()

